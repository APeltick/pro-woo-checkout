<?php
/*
Plugin Name: pro-woo-check
Plugin URI: http://test.com
Description: edit new info field in woo checkout
Version: 0.0.1
Author: Alex
Author URI: http://test.com
*/

add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );

function my_custom_checkout_field( $checkout ) {

	echo '<div id="my_custom_checkout_field"><h2>' . __('My Field') . '</h2>';

	woocommerce_form_field( 'my_field_name', array(
		'type'          => 'text',
		'class'         => array('my-field-class form-row-wide'),
		'label'         => __('Fill in this field'),
		'placeholder'   => __('Enter something'),
	), $checkout->get_value( 'my_field_name' ));

	echo '</div>';

}

/**
 * Do your field required
 */
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process() {
	// Check if set, if its not set add an error.
	if ( ! $_POST['my_field_name'] )
		wc_add_notice( __( 'Please enter something into this new shiny field.' ), 'error' );
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
	if ( ! empty( $_POST['my_field_name'] ) ) {
		update_post_meta( $order_id, 'my_field', sanitize_text_field( $_POST['my_field_name'] ) );
	}
}


/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta( $order ){
	echo '<p><strong>'.__('My Field').':</strong> ' . get_post_meta( get_the_ID( $order ), 'my_field', true ) . '</p>';
}

/**
 * Display field value on the email page
 */
add_filter('woocommerce_email_order_meta_keys', 'my_custom_order_meta_keys');

function my_custom_order_meta_keys( $keys ) {
	$keys[] = 'my_field';
	return $keys;
}
